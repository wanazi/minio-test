package com.tqd.test;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.lifecycle.LifecycleFilter;
import com.amazonaws.services.s3.model.lifecycle.LifecyclePrefixPredicate;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AmazonS3Test {
    private static AmazonS3 s3Client;

    @BeforeAll
    public static void setup(){
        AWSCredentials credentials = new BasicAWSCredentials("minioadmin", "minioadmin");
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setSignerOverride("AWSS3V4SignerType");

        s3Client = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:9000", Regions.CN_NORTH_1.name()))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfiguration)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }
    @Test
    public void testSetBucketLifecycle(){

        s3Client.listBuckets().forEach(bucket -> {
            System.out.println(bucket.getName());
        });
        BucketLifecycleConfiguration config=new BucketLifecycleConfiguration();
        List<BucketLifecycleConfiguration.Rule> rules=new ArrayList<>();
        BucketLifecycleConfiguration.Rule rule=new BucketLifecycleConfiguration.Rule();
        rule.withExpirationInDays(7)
                .withStatus(BucketLifecycleConfiguration.ENABLED)
                .withFilter(new LifecycleFilter().withPredicate(new LifecyclePrefixPredicate("tmp/xx1")));
        rules.add(rule);
        config.setRules(rules);
        s3Client.setBucketLifecycleConfiguration("test",config);
    }

    @Test
    public void testUploadFile() throws MalformedURLException {

        PutObjectResult result=s3Client.putObject("test","",new File("./pom.xml"));
        System.out.println(result);
    }
}
