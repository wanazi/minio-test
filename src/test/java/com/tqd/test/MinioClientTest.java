package com.tqd.test;

import com.amazonaws.services.s3.model.BucketPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import io.minio.*;
import io.minio.errors.*;
import io.minio.http.Method;
import io.minio.messages.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class MinioClientTest {

    private static MinioClient c;
    @BeforeAll
    public static void setup() throws MalformedURLException {
        c=MinioClient.builder()
                .endpoint(new URL("http://localhost:9000"))
                .credentials("minioadmin","minioadmin")
                .build();
    }

    @Test
    public void createBucket() throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        c.makeBucket(MakeBucketArgs.builder()
                .bucket("test")
                .build());

    }
    @Test
    public void listObjects(){
        c.listObjects(ListObjectsArgs.builder()
                .bucket("test")
                .build()).forEach(object->{
            try {
                System.out.println(object.get().objectName());
            } catch (Throwable e) {
                e.printStackTrace();

            }
        });
    }
    Gson gson=new GsonBuilder().setPrettyPrinting().create();
    @Test
    public void getBucketPolicy() throws ServerException, InsufficientDataException, ErrorResponseException, BucketPolicyTooLargeException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        var policy=c.getBucketPolicy(GetBucketPolicyArgs.builder()
                        .bucket("test")
                .build());
        policy=gson.toJson(gson.fromJson(policy, JsonElement.class));
        System.out.println(policy);
    }

    @Test
    public void setAsPublic() throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        BucketPolicy bp;
        String config="""
                       {
                           "Version": "2012-10-17",
                           "Statement": [
                             {
                               "Effect": "Allow",
                               "Principal": {
                                 "AWS": [
                                   "*"
                                 ]
                               },
                               "Action": [
                                 "s3:GetBucketLocation",
                                 "s3:ListBucket",
                                 "s3:ListBucketMultipartUploads"
                               ],
                               "Resource": [
                                 "arn:aws:s3:::test"
                               ]
                             },
                             {
                               "Effect": "Allow",
                               "Principal": {
                                 "AWS": [
                                   "*"
                                 ]
                               },
                               "Action": [
                                 "s3:DeleteObject",
                                 "s3:GetObject",
                                 "s3:ListMultipartUploadParts",
                                 "s3:PutObject",
                                 "s3:AbortMultipartUpload"
                               ],
                               "Resource": [
                                 "arn:aws:s3:::test/*"
                               ]
                             }
                           ]
                         }
                         
                         Process finished with exit code 0
                         
                """;
        c.setBucketPolicy(SetBucketPolicyArgs.builder()
                .config(config)
                .bucket("test")
                .build());

    }
    @Test
    public void createShareLink() throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        String url=c.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                .bucket("test")
                .object("/tmp/pom.xml")
                .method(Method.GET)
                .expiry(1, TimeUnit.DAYS)
                .build());
        System.out.println(url);
    }

    @Test
    public void testUploadObject() throws IOException, ServerException, InsufficientDataException, ErrorResponseException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        ObjectWriteResponse resp=c.uploadObject(UploadObjectArgs.builder()
                .bucket("test")
                .object("/tmp/pom1.xml")
                .filename("pom.xml")
                .build());
        System.out.println(resp);
    }

    @Test
    public void setBucketLifecycle() throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        LifecycleRule rule= new LifecycleRule(
                Status.ENABLED,
                null,
                new Expiration((ResponseDate) null,7,null),
                new RuleFilter(null,"tmp/",null),
                null,
                null,
                null,
                null
        );

        GetBucketLifecycleArgs args1= GetBucketLifecycleArgs.builder().bucket("test").build();

        LifecycleConfiguration configuration=new LifecycleConfiguration(Arrays.asList(rule));
        String config=new GsonBuilder()
                .setPrettyPrinting()
                .setFieldNamingStrategy(field->{
                    Element e=field.getAnnotation(Element.class);
                    if(e!=null){
                        return e.name();
                    }
                    ElementList el=field.getAnnotation(ElementList.class);
                    if(el!=null){
                        return el.name();
                    }
                    return  field.getName();
                })
                .create()
                .toJson(configuration);
        System.out.println(config);
        SetBucketLifecycleArgs bucketLifecycle=SetBucketLifecycleArgs.builder()
                .bucket("test")
                .config(configuration)
                .build();
        c.setBucketLifecycle(bucketLifecycle);
    }
}
