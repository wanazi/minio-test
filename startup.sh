#!/bin/bash

docker run -ti --rm \
  -p 9001:9001 \
  -p 9000:9000 \
  -e MINIO_ROOT_USER="minioadmin" \
  -e MINIO_ROOT_PASSWORD="minioadmin" \
  -v $(pwd)/data:/data \
   minio/minio:RELEASE.2021-12-20T22-07-16Z \
   server /data --console-address ":9001"
